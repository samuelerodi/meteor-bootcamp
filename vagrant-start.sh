cd /vagrant

BASE=/home/`whoami`

echo "Mounting virtual directories to avoid symlink error to user directory"

# Migrate folders to home folder to avoid simlink errors
# declare -a folders=("node_modules" "data" "tmp")
declare -a folders=("node_modules")

## now loop through the above array
for i in "${folders[@]}"
do
  if [ ! -d $BASE/$i ]; then
    # Control will enter here if directory doesn't exists.
    echo "WARNING: $BASE/$i not found. Creating directory..."
    mkdir $BASE/$i
  fi
  if [ ! -d /vagrant/$i ]; then
    # Control will enter here if directory doesn't exists.
    echo "WARNING: /vagrant/$i not found. Creating directory..."
    mkdir /vagrant/$i
  fi
  echo "$i. Mounting $i virtual directory..."
  sudo mount --bind $BASE/$i /vagrant/$i/
done

if [ ! -d $BASE/meteor ]; then
  # Control will enter here if directory doesn't exists.
  mkdir $BASE/meteor
fi
if [ ! -d /vagrant/.meteor/local ]; then
  # Control will enter here if directory doesn't exists.
  mkdir /vagrant/.meteor/local
fi

sudo mount --bind $BASE/meteor /vagrant/.meteor/local
