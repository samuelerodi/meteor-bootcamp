#!/usr/bin/env bash
##Cache jdk
apt-cache search jdk

BASE=/home/`whoami`

echo "Installing Android SDK..."
### Install Android SDK
# create sdk folder
export ANDROID_HOME=$BASE/android-sdk
mkdir -p $ANDROID_HOME

# download android sdk
cd $ANDROID_HOME
wget https://dl.google.com/android/repository/tools_r25.2.3-linux.zip
unzip tools_r25.2.3-linux.zip

# install all sdk packages
echo y | sudo ./tools/android update sdk --no-ui --filter platform-tools,build-tools,android-25,android-26,android-27,android-28
echo y | sudo ./tools/android update sdk --no-ui --all --filter "build-tools-26.0.2"
# set path
export PATH=${PATH}:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$ANDROID_HOME/build-tools/25.0.2/
source /etc/profile





echo "Mounting virtual directories to avoid symlink error to user directory"

# Migrate folders to home folder to avoid simlink errors
# declare -a folders=("node_modules" "data" "tmp")
declare -a folders=("node_modules")

## now loop through the above array
for i in "${folders[@]}"
do
  # Remove folder if already present
  rm -rf /vagrant/$i
  mkdir /vagrant/$i
  if [ ! -d $BASE/$i ]; then
    # Control will enter here if directory doesn't exists.
    mkdir $BASE/$i
  fi
  echo "$i. Mounting $i virtual directory..."
  sudo mount --bind $BASE/$i /vagrant/$i/
done

rm -rf /vagrant/.meteor/local
mkdir /vagrant/.meteor/local
if [ ! -d $BASE/meteor ]; then
  # Control will enter here if directory doesn't exists.
  mkdir $BASE/meteor
fi
sudo mount --bind $BASE/meteor /vagrant/.meteor/local


echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> $BASE/.profile
echo "export ANDROID_HOME=$BASE/android-sdk" >> $BASE/.profile
echo "export PATH=$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin" >> $BASE/.profile
echo "cd /vagrant" >> $BASE/.profile

source $BASE/.profile

meteor npm i
# meteor
# echo "Setting up meteor distribution..."
echo "Done!"
