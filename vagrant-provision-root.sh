#!/usr/bin/env bash

echo "Hi! I am `whoami`"
echo "Updating VM..."
apt-get update
apt-get install unzip
echo "Downloading Meteor..."
curl https://install.meteor.com/ | sh

echo "Installing JDK..."
apt-get install openjdk-8-jdk -y
echo "Installing Gradle..."
apt-get install gradle -y
echo "I'm done!"
